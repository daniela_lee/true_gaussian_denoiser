import scipy.io as sio
import numpy as np
from google.colab import drive
import os, glob, datetime, time
import matplotlib.pyplot as plt

# result_dir = os.path.join('/content/gdrive/My Drive/Deep_Learning/Gaussian_models/perceptive', 'DnCNN_DilConv3Depth8lr1e-3','results')



def get_results_list(result_dir,result_file,set_cur,iteracoes):
  
    x = [ i for i in range(1,iteracoes+1)]   
    
    with open(os.path.join(result_dir,'results',set_cur,'psnr_result.txt'), 'r') as file:
          metrics_list = []
          for line in file:           
             metrics_list.append(float(line))
    print (metrics_list)         
    return metrics_list

if __name__ == '__main__':

  result_dir = os.path.join('true_gaussian_denoiser', 'models')
  models = ['DnCNN_DilConv3Depth8lr1e-3_sigma25','DnCNN_Original2_sigma25']
  set_names = ['Set68', 'Set12']
  iteracoes = 30  
  
  #Gráficos PSNR  
  
  for set_cur in set_names:
      
      for model in models:
          psnr_all = get_results_list(os.path.join(result_dir,model),'psnr_result.txt',set_cur,iteracoes)
          plt.plot(x, psnr_all, label=model)
      
      plt.plot()

      plt.xlabel("iteracoes")
      plt.ylabel("valor psnr")
      plt.title("PSNR para "+set_cur)
      plt.legend()
      plt.savefig('/content/gdrive/My Drive/Deep_Learning/Gaussian_models/perceptive/psnr_'+set_cur+'.png')
      plt.show()