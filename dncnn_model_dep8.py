# -*- coding: utf-8 -*-

# PyTorch 0.4.1, https://pytorch.org/docs/stable/index.html
import torch
import torch.nn as nn
import torch.nn.init as init
from dilatedConv import DilatedConv2dBlock

class DnCNN(nn.Module):
    
    #experimento 1 = depth 8 (dilated conv)
    def __init__(self, depth=8, n_channels=64, image_channels=1, use_bnorm=True, kernel_size=3):
#     def __init__(self, depth=17, n_channels=64, image_channels=1, use_bnorm=True, kernel_size=3):
        super(DnCNN, self).__init__()
        
        kernel_size = 3
        padding = 1
        layers = []

#         layers.append(nn.Conv2d(in_channels=image_channels, out_channels=n_channels, kernel_size=kernel_size, padding=padding, bias=True))
        layers.append(DilatedConv2dBlock(inputs=image_channels, outputs=n_channels, padding=1, bias=True))
        layers.append(nn.ReLU(inplace=True))
        for _ in range(depth-2):
#             layers.append(nn.Conv2d(in_channels=n_channels, out_channels=n_channels, kernel_size=kernel_size, padding=padding, bias=False))
            layers.append(DilatedConv2dBlock(inputs=n_channels, outputs=n_channels))
            layers.append(nn.BatchNorm2d(n_channels, eps=0.0001, momentum = 0.95))
            layers.append(nn.ReLU(inplace=True))
      
#         layers.append(nn.Conv2d(in_channels=n_channels, out_channels=image_channels, kernel_size=kernel_size, padding=padding, bias=False))
        layers.append(DilatedConv2dBlock(inputs=n_channels, outputs=image_channels))
        self.dncnn = nn.Sequential(*layers)
        self._initialize_weights()

    def forward(self, x): 
      ##Original
        y = x
        out = self.dncnn(x)
        return y-out
 
    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                init.orthogonal_(m.weight)
                print('init weight')
                if m.bias is not None:
                    init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                init.constant_(m.weight, 1)
                init.constant_(m.bias, 0)
